﻿using System;
using System.Net.Sockets;
using System.Text;

namespace Client;

internal class Program
{
    public const int defaultPort = Server.Program.defaultPort;
    public static string defaultIpAddress = "127.0.0.1";

    private static void Main(string[] args)
    {
        var arguments = CliArguments.Parse(args);
        TcpClient client = new TcpClient(arguments.ipAddress, arguments.port);
        Console.WriteLine("Підключення до сервера...");
        Console.WriteLine($"port: {arguments.port}, ip: {arguments.ipAddress}");

        NetworkStream clientStream = client.GetStream();

        while (true)
        {
            Console.Write("Client > ");
            string message = Console.ReadLine();

            byte[] sendMessage = Encoding.ASCII.GetBytes(message);
            clientStream.Write(sendMessage, 0, sendMessage.Length);
            clientStream.Flush();

            byte[] receivedMessage = new byte[Server.Program.messageSize];
            int bytesRead = clientStream.Read(receivedMessage, 0, 4096);

            Console.WriteLine($"Сервер повідомляє: {Encoding.UTF8.GetString(receivedMessage, 0, bytesRead)}");
        }

        client.Close();
    }
}