﻿using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;

namespace Server;

public class Program
{
    static TcpListener listener;
    public const int defaultPort = 8888;
    public static IPAddress defaultIpAddress => IPAddress.Any;
    public const int messageSize = 4096;
    public static ServerDbContext context;

    private static void Main(string[] args)
    {
        var arguments = CliArguments.Parse(args);

        var serverThread = StartServer(arguments);
    }

    static Thread FindAndListenToClient(TcpListener listener)
    {
        TcpClient client = listener.AcceptTcpClient();
        Thread clientThread = new Thread(HandleClient);
        clientThread.Start(client);
        return clientThread;
    }

    static Thread StartServer(CliArguments arguments)
    {
        Console.WriteLine("Запуск серверу..");
        listener = new TcpListener(arguments.ipAddress, arguments.port);
        listener.Start();
        Console.WriteLine($"port: {arguments.port}, ipAddress: {arguments.ipAddress}");

        context = new ServerDbContext();
        
        var clientThread = FindAndListenToClient(listener);
        return clientThread;
    }

    static void SendMessageToClient(string message, NetworkStream clientStream)
    {
        message = "Server: " + message;
        byte[] byteArray = Encoding.UTF8.GetBytes(message);
        clientStream.Write(byteArray, 0, byteArray.Length);
        clientStream.Flush();
    }

    static void HandleClient(object obj)
    {
        TcpClient tcpClient = (TcpClient)obj;
        NetworkStream clientStream = tcpClient.GetStream();

        byte[] message = new byte[messageSize];
        int bytesRead;

        while (true)
        {
            bytesRead = 0;

            try
            {
                bytesRead = clientStream.Read(message, 0, messageSize);
            }
            catch
            {
                break;
            }

            if (bytesRead == 0)
                break;

            string receivedMessage = Encoding.ASCII.GetString(message, 0, bytesRead);
            Console.WriteLine($"Отримано: {receivedMessage}");

            int number = 0;
            try
            {
                Console.WriteLine("parsing");
                number = int.Parse(receivedMessage);
            }
            catch (System.Exception)
            {
                string error_message = "Не вдалося зчитати число";
                Console.WriteLine(error_message);
                SendMessageToClient(error_message, clientStream);
                return;
            }

            var user = context.Users.FirstOrDefault(u => u.Id == number);

            if (user != null)
            {
                SendMessageToClient(user.Info, clientStream);
            }
            else
            {
                SendMessageToClient("Record was not found", clientStream);
            }
        }

        tcpClient.Close();
    }
}