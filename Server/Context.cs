using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace Server;

public class ServerDbContext : DbContext
{
    public DbSet<User> Users { get; set; }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
        optionsBuilder.UseSqlite("Data Source=./Database.db")
        .LogTo(Console.WriteLine, LogLevel.Information);
    }
}