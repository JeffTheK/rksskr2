using System.Net;
using System.Net.Sockets;

namespace Server;

public struct CliArguments
{
    public int port;
    public IPAddress ipAddress;

    public class CliArgumentsParseException : Exception
    {
        public CliArgumentsParseException(Exception except) : base($"Помилка під час читання аргументів командного рядка: {except}")
        {
        }
    }

    public CliArguments(int port, IPAddress ipAddress)
    {
        this.port = port;
        this.ipAddress = ipAddress;
    }

    public static CliArguments Parse(string[] args) {
        int? port = null;
        IPAddress? ipAddress = null;

        try
        {
            for (int i = 0; i < args.Length; i++) {
                if (args[i] == "-p" || args[i] == "--port") {
                    port = int.Parse(args[i + 1]);
                } else if (args[i] == "-ip") {
                    ipAddress = IPAddress.Parse(args[i + 1]);
                }
            }
        }
        catch (Exception except)
        {
            throw new CliArgumentsParseException(except);
        }

        if (!port.HasValue) {
            port = Program.defaultPort;
        }
        if (ipAddress == null) {
            ipAddress = Program.defaultIpAddress;
        }

        return new CliArguments(port.Value, ipAddress);
    }
}